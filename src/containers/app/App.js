import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Header from "../../components/Header/Header";
import Dishes from "../dishes/dishes";
import Orders from "../../components/Orders/Orders";
import AddDish from "../../components/AddDish/AddDish";
import EditDish from "../../components/EditDish/EditDish";

const App = () => {
  return (
      <div className="container">
          <BrowserRouter>
              <Route component={Header}/>
              <Switch>
                  <Route path="/" exact component={Dishes}/>
                  <Route path="/orders" exact component={Orders}/>
                  <Route path="/addDish" exact component={AddDish}/>
                  <Route path="/editDish" exact component={EditDish}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
};

export default App;
