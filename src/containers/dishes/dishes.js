import React, {useEffect} from 'react';
import './dishes.css';
import {useDispatch, useSelector} from "react-redux";
import {deleteDish, fetchDishes} from "../../store/actions";

const Dishes = props => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.get.dishes);

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const addDishPage = () => {
        props.history.push('/addDish');
    };

    const deleteDishHandler = async event => {
        await deleteDish(event.target.id);
        dispatch(fetchDishes());
    };

    const editDishPage = (event) => {
        const params = new URLSearchParams({key: event.target.value});
        props.history.push({
            pathname: '/editDish',
            search: '?' + params.toString(),
        });
    };

    if (dishes !== null) {
        return (
            <div className="dishesPage">
                <div className="top">
                    <h3>Dishes</h3>
                    <button onClick={addDishPage} className="addDishBtn">Add new Dish</button>
                </div>
                <div className="dishes">
                    {
                        Object.keys(dishes).map(key => {
                            return (
                                <div className="dish" key={key}>
                                    <img className="dishImg" src={dishes[key].image} alt="dish"/>
                                    <p className="dishName">{dishes[key].title}</p>
                                    <p className="dishPrice">{dishes[key].price} KGS</p>
                                    <div className="dishButtons">
                                        <button type="button" onClick={editDishPage} value={key} className="editDish">Edit</button>
                                        <button type="button" onClick={deleteDishHandler} className="deleteDish" id={key}>Delete</button>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    } else {
        return null;
    }
};

export default Dishes;