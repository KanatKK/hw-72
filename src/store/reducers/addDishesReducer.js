import {ADD_IMAGE, ADD_PRICE, ADD_TITLE,} from "../actionTypes";

const initialState = {
    title: '',
    price: '',
    image: '',
}

const addDishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TITLE:
            return {...state, title: action.value};
        case ADD_PRICE:
            return {...state, price: action.value};
        case ADD_IMAGE:
            return {...state, image: action.value};
        default:
            return  state;
    }
};

export default addDishesReducer;