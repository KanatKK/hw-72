import {EDIT_IMAGE, EDIT_PRICE, EDIT_TITLE,} from "../actionTypes";

const initialState = {
    title: '',
    price: '',
    image: '',
};

const editDishReducer = (state=initialState, action) => {
    switch (action.type) {
        case EDIT_TITLE:
            return {...state, title: action.value};
        case EDIT_PRICE:
            return {...state, price: action.value};
        case EDIT_IMAGE:
            return {...state, image: action.value};
        default:
            return state;
    }
};

export default editDishReducer;