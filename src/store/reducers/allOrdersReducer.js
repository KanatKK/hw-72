import {CLEAR_ORDERS, GET_ORDER, GET_ORDERS} from "../actionTypes";

const initialState = {
  orders: null,
  order: [],
};

const allOrdersReducer = (state=initialState, action) => {
  switch (action.type) {
    case GET_ORDERS:
      return {...state, orders: action.value};
    case GET_ORDER:
      return {...state, orders: state.order.push(action.value)};
    case CLEAR_ORDERS:
      return {...state, orders: state.order.length = 0};
    default:
      return state
  }
};

export default allOrdersReducer;