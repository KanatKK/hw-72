import {GET_DISHES} from "../actionTypes";

const initialState = {
    dishes: null,
};

const dishesReducer = (state=initialState, action) => {
    switch (action.type) {
        case GET_DISHES:
            return {...state, dishes: action.value};
        default:
            return  state;
    }
};

export default dishesReducer;