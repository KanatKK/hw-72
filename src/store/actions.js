import {
    ADD_IMAGE,
    ADD_PRICE,
    ADD_TITLE, CLEAR_ORDERS, COMPLETE_ORDER,
    EDIT_IMAGE,
    EDIT_PRICE,
    EDIT_TITLE,
    GET_DISHES, GET_ORDER, GET_ORDERS,
} from "./actionTypes";
import axios from 'axios';

export const getDishes = value => {
    return {type: GET_DISHES, value};
};

export const addTitle = value => {
    return {type: ADD_TITLE, value};
};
export const addPrice = value => {
    return {type: ADD_PRICE, value};
};
export const addImage = value => {
    return {type: ADD_IMAGE, value};
};

export const editDishTitle = value => {
    return {type: EDIT_TITLE, value};
};
export const editDishPrice = value => {
    return {type: EDIT_PRICE, value};
};
export const editDishImage = value => {
    return {type: EDIT_IMAGE, value};
};

const getOrders = value => {
    return {type: GET_ORDERS, value};
};
export const getOrder = value => {
    return {type: GET_ORDER, value};
};
export const clearOrders = () => {
    return {type: CLEAR_ORDERS};
};

export const fetchDishes = () => {
    return async dispatch => {
        try {
            const response = await axios.get('https://pizzaproject-ec15f.firebaseio.com/dishes.json');
            dispatch(getDishes(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addNewDish = async value => {
    try {
        await axios.post('https://pizzaproject-ec15f.firebaseio.com/dishes.json', value);
    } catch(e) {
        console.log(e);
    }
};

export const deleteDish = async value => {
    await axios.delete('https://pizzaproject-ec15f.firebaseio.com/dishes/'+value+'.json');
};

export const completeOrder = async value => {
    await axios.delete('https://pizzaproject-ec15f.firebaseio.com/orders/'+value+'.json')
}

export const editDish = (value) => {
    return async dispatch => {
        try {
            const response = await axios.get('https://pizzaproject-ec15f.firebaseio.com/dishes/'+value+'.json');
            dispatch(editDishTitle(response.data.title));
            dispatch(editDishPrice(response.data.price));
            dispatch(editDishImage(response.data.image));
        } catch(e) {
            console.log(e);
        }
    };
};

export const fetchOrders = () => {
    return async dispatch => {
        try {
            const response = await axios.get('https://pizzaproject-ec15f.firebaseio.com/orders.json');
            dispatch(getOrders(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

