export const GET_DISHES = 'GET_DISHES';

export const ADD_TITLE = 'ADD_TITLE';
export const ADD_IMAGE = 'ADD_IMAGE';
export const ADD_PRICE = 'ADD_PRICE';

export const EDIT_TITLE = 'EDIT_TITLE';
export const EDIT_PRICE = 'EDIT_PRICE';
export const EDIT_IMAGE = 'EDIT_IMAGE';

export const GET_ORDERS = 'GET_ORDERS';
export const GET_ORDER = 'GET_ORDER';
export const CLEAR_ORDERS = 'CLEAR_ORDERS'