import React from 'react';
import './Header.css';

const Header = props => {
    const switchToDishes = () => {
        props.history.push('/');
    };

    const switchToOrders = () => {
        props.history.push('/orders');
    };

    return (
        <header>
            <h2>Turtle Pizza Admin</h2>
            <div className="switcher">
                <p onClick={switchToDishes}>Dishes</p>
                 |
                <p onClick={switchToOrders}>Orders</p>
            </div>
        </header>
    );
};

export default Header;