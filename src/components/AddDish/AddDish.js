import React from 'react';
import './AddDish.css';
import {useDispatch, useSelector} from "react-redux";
import {addImage, addNewDish, addPrice, addTitle} from "../../store/actions";

const AddDish = props => {
    const title = useSelector(state => state.add.title);
    const price = useSelector(state => state.add.price);
    const image = useSelector(state => state.add.image);
    const mainState = useSelector(state => state.add);
    const dispatch = useDispatch();

    const addTitleHandler = event => {
        dispatch(addTitle(event.target.value));
    };

    const addPriceHandler = event => {
        dispatch(addPrice(event.target.value));
    };

    const addImageHandler = event => {
        dispatch(addImage(event.target.value));
    };

    const addNewDishHandler = async () => {
        await addNewDish({...mainState});
        dispatch(addTitle(''));
        dispatch(addPrice(''));
        dispatch(addImage(''));
        props.history.push('/');
    };

    return (
        <div className="add">
            <h3>Edit Dish</h3>
            <label>
                <p>Name:</p>
                <input onChange={addTitleHandler} value={title} type="text"/>
            </label>
            <label>
                <p>Price:</p>
                <input onChange={addPriceHandler} value={price} type="text"/>
            </label>
            <label>
                <p>Image</p>
                <input onChange={addImageHandler} value={image} type="text"/>
            </label>
            <button type="button" onClick={addNewDishHandler}>Add</button>
        </div>
    );
};

export default AddDish;