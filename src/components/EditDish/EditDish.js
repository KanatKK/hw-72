import React, {useEffect, useState} from 'react';
import './EditDish.css';
import {useDispatch, useSelector} from "react-redux";
import {editDish,} from "../../store/actions";
import axios from 'axios';

const EditDish = props => {
    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };

    const [mainState, setMainState] = useState({
        title: '', price: '',image: '',
    });
    const stateCopy = {...mainState};

    const dispatch = useDispatch();
    const title = useSelector(state => state.edit.title);
    const price= useSelector(state => state.edit.price);
    const image = useSelector(state => state.edit.image);

    const stateValue = () => {
        stateCopy.title = title;
        stateCopy.price = price;
        stateCopy.image = image;
        setMainState(stateCopy);
    }

    useEffect(() => {
        const editDishHandler = async () => {
            await dispatch(editDish(getParams().key));
        };
        editDishHandler().catch(e =>{console.log(e)});
    }, []);

    useEffect(() => {
        stateValue();
    },[title, price, image]);

    const stateDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setMainState(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const addEditedDish = async () => {
        try{
            await axios.put('https://pizzaproject-ec15f.firebaseio.com/dishes/'+getParams().key+'.json', {...mainState});
        } finally {
            props.history.push('/');
        }
    };

    if (title !== null) {
        return (
            <div className="edit">
                <h3>Edit Dish</h3>
                <label>
                    <p>Name:</p>
                    <input name="title" onChange={stateDataChanger} value={mainState.title} type="text"/>
                </label>
                <label>
                    <p>Price:</p>
                    <input name="price" onChange={stateDataChanger} value={mainState.price} type="text"/>
                </label>
                <label>
                    <p>Image</p>
                    <input name="image" onChange={stateDataChanger} value={mainState.image} type="text"/>
                </label>
                <button onClick={addEditedDish} type="button">Add</button>
            </div>
        );
    } else {
        return null
    }

};

export default EditDish;