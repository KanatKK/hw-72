import React, {useEffect} from 'react';
import './Orders.css';
import {useDispatch, useSelector} from "react-redux";
import {clearOrders, completeOrder, fetchOrders, getOrder} from "../../store/actions";
import axios from 'axios';

const Orders = props => {
    const dispatch = useDispatch();
    const allOrders = useSelector(state=> state.ord.orders);
    const orders = useSelector(state=> state.ord.order);

    useEffect(() => {
        dispatch(fetchOrders());
    }, [dispatch]);

    const completeOrderHandler = async event => {
        await completeOrder(event.target.id);
        props.history.push('/');
    };

    if (allOrders !== null) {
        Object.keys(allOrders).map(async key => {
            Object.keys(allOrders[key]).map(async item => {
                dispatch(clearOrders());
                const response = await axios.get('https://pizzaproject-ec15f.firebaseio.com/dishes/'+item+'.json');
                const price = Number(response.data.price * allOrders[key][item])
                dispatch(getOrder({[key]: {name: response.data.title, priceTag: price ,quantity: allOrders[key][item]}}))
            });
        });
        if (orders.length !==  0) {
            return (
                <div className="ordersPage">
                    {
                        Object.keys(orders).map(item => {
                            return (
                                Object.keys(orders[item]).map(key => {
                                    return (
                                        <div className="order" key={Date.now()}>
                                            <div className="info">
                                                <span className="orderInfo">{orders[item][key].name}</span>
                                                <span
                                                    className="orderInfo" style={{textAlign: 'center'}}
                                                >X{orders[item][key].quantity}
                                                </span>
                                                <span
                                                    className="orderInfo" style={{textAlign: 'center'}}
                                                >{orders[item][key].priceTag}
                                                </span>
                                            </div>
                                            <div className="delivery">
                                                <span className="deliveryInfo">Delivery:</span>
                                                <span className="deliveryInfo" style={{textAlign: 'center'}}>150 KGS</span>
                                                <span className="deliveryInfo" style={{textAlign: 'center'}}>
                                                    Total price: {Number(orders[item][key].priceTag + 150)}
                                                </span>
                                            </div>
                                            <button id={key} onClick={completeOrderHandler} className="completeOrder">Complete Order</button>
                                        </div>
                                    )
                                })
                            )

                        })
                    }
                </div>
            );
        } else {return null}
    } else {
        return null
    }
};

export default Orders;