import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";
import {Provider} from "react-redux";
import dishesReducer from "./store/reducers/dishesReducer";
import './index.css';
import App from './containers/app/App';
import * as serviceWorker from './serviceWorker';
import addDishesReducer from "./store/reducers/addDishesReducer";
import editDishReducer from "./store/reducers/editDishReducer";
import allOrdersReducer from "./store/reducers/allOrdersReducer";

const rootReducers = combineReducers({
    get: dishesReducer,
    add: addDishesReducer,
    edit: editDishReducer,
    ord: allOrdersReducer,
});

const store = createStore(rootReducers, applyMiddleware(thunkMiddleware));

ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
